import Papel from '../models/Papel';

interface CreatePapel {
    id_papel: string;
    chave_papel: string; 
    descricao_papel: string;
}

class PapeisRepository {
    private papeis: Papel[];

    constructor() {
        this.papeis = [];
    }

    public all(): Papel[] {
        return this.papeis;
    }

    public create({ id_papel, chave_papel, descricao_papel }: CreatePapel): Papel {
        const papel = new Papel({
            id_papel,
            chave_papel,
            descricao_papel
        });
        this.papeis.push(papel);

        return papel;
    }
}

export default PapeisRepository;