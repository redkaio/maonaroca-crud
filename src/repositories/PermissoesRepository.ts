import { isThisTypeNode } from 'typescript';
import Permissao from '../models/Permissao';

interface CreatePermissao {
    chave_permissao: string;
    descricao_permissao: string;
    id_permissao: string;

}

class PermissoesRepository {
    private permissoes: Permissao[];

    constructor() {
        this.permissoes = [];
    }

    public all(): Permissao[] {
        return this.permissoes;
    }

    public create({ chave_permissao, id_permissao, descricao_permissao }: CreatePermissao): Permissao {
        const permissao = new Permissao({
            chave_permissao,
            descricao_permissao,
            id_permissao
        });
        this.permissoes.push(permissao);
        
        return permissao;
    }
}

export default PermissoesRepository;