import Acao from '../models/Acao';

interface CreateAcao {
    id_acao: string;
    chave_acao: string;
    nivel_acao: string;
}

class AcoesRepository {
    private acoes: Acao[];

    constructor() {
        this.acoes = [];
    }

    public all(): Acao[] {
        return this.acoes;
    }

    public create({ id_acao, chave_acao, nivel_acao }: CreateAcao): Acao {
        const acao = new Acao({
            id_acao,
            chave_acao,
            nivel_acao
        });
        this.acoes.push(acao);

        return acao;
    }
}

export default AcoesRepository;