import Usuario from '../models/Usuario';

interface CreateUsuario {
    nome: string;
    cpf: string;
    email: string;
    telefone: string;
}

class UsuariosRepository {
    private usuarios: Usuario[];

    constructor() {
        this.usuarios = [];
    }

    public all(): Usuario[] {
        return this.usuarios;
    }

    public create ({ nome, email, cpf, telefone }: CreateUsuario): Usuario {
        const usuario = new Usuario({
            nome,
            email,
            cpf,
            telefone,
        });
        this.usuarios.push(usuario);

        return usuario;
    }
}

export default UsuariosRepository;