import { uuid } from 'uuidv4';

interface AcaoConstructor {
    id_acao: string;
    chave_acao: string;
    nivel_acao: string;
}

class Acao {
    id_acao: string;
    chave_acao: string;
    nivel_acao: string;

    constructor({ id_acao, chave_acao, nivel_acao }) {
        this.id_acao = uuid();
        this.chave_acao = chave_acao;
        this.nivel_acao = nivel_acao;
    }
}

export default Acao;