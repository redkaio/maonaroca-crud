import { uuid } from 'uuidv4';

interface UsuarioConstructor {
    nome: string;
    cpf: string;
    email: string;
    telefone: string;
    id_firebase: string;
}

class Usuario {
    nome: string;
    cpf: string;
    email: string;
    telefone: string;
    id_firebase: string;

    constructor({ nome, cpf, email, telefone }: Omit<Usuario, 'id_firebase'>){
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.telefone = telefone;
        this.id_firebase = uuid();
    }
}

export default Usuario;

