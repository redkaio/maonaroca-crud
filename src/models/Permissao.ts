import { uuid } from 'uuidv4'

class Permissao {
    chave_permissao: string;
    descricao_permissao: string;
    id_permissao: string;

    constructor ({ chave_permissao, descricao_permissao, id_permissao }) {
        this.chave_permissao = chave_permissao;
        this.descricao_permissao = descricao_permissao;
        this.id_permissao = id_permissao;
    }
}

export default Permissao;