import { uuid } from 'uuidv4';

interface PapelConstructor{
    id_papel: string;
    chave_papel: string; 
    descricao_papel: string;
}

class Papel {
    id_papel: string;
    chave_papel: string; 
    descricao_papel: string;

    constructor ({ id_papel, chave_papel, descricao_papel }) {
        this.id_papel = uuid();
        this.chave_papel = chave_papel;
        this.descricao_papel = descricao_papel;
    }
}

export default Papel; 