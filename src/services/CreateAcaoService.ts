import Acao from '../models/Acao';
import AcoesRepository from '../repositories/AcoesRepository';

interface Request {
    id_acao: string;
    chave_acao: string;
    nivel_acao: string;
}

class CreateAcaoService {
    private acoesRepository: AcoesRepository;

    constructor(acoesRepository: AcoesRepository){
        this.acoesRepository = acoesRepository;
    }

    public execute({ id_acao, chave_acao, nivel_acao }: Request): Acao {
        const acao = this.acoesRepository.create({
            id_acao,
            chave_acao,
            nivel_acao,
        });
        return acao;
    } 
}
export default CreateAcaoService;
