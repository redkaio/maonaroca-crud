import Usuario from '../models/Usuario';
import UsuariosRepository from '../repositories/UsuariosRepository';

interface Request {
    nome: string;
    cpf: string;
    email: string;
    telefone: string;
}

class CreateUsuarioService {
    private usuariosRepository: UsuariosRepository;

    constructor(usuariosRepository: UsuariosRepository){
        this.usuariosRepository = usuariosRepository;
    }

    public execute({ nome, cpf, telefone, email }: Request): Usuario {
        const usuario = this.usuariosRepository.create({
            nome,
            cpf,
            email,
            telefone,
        });
        return usuario;
    }
}
export default CreateUsuarioService;