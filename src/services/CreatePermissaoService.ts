import Permissao from '../models/Permissao';
import PermissoesRepository from '../repositories/PermissoesRepository';

interface Request {
    chave_permissao: string;
    descricao_permissao: string;
    id_permissao: string;
}

class CreatePermissaoService {
    private permissoesRepository: PermissoesRepository;
    
    constructor(permissoesRepository: PermissoesRepository){
        this.permissoesRepository = permissoesRepository;
    }

    public execute({ chave_permissao, descricao_permissao, id_permissao }: Request): Permissao {
        const permissao = this.permissoesRepository.create({
            chave_permissao,
            descricao_permissao,
            id_permissao
        });
        return permissao;
    } 
}

export default CreatePermissaoService;