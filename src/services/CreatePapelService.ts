import Papel from '../models/Papel';
import PapeisRepository from '../repositories/PapeisRepository';

interface Request {
    id_papel: string;
    chave_papel: string; 
    descricao_papel: string;
}

class CreatePapelService {
    private papeisRepository: PapeisRepository;

    constructor(papeisRepository: PapeisRepository){
        this.papeisRepository = papeisRepository;
    }

    public execute({ id_papel, chave_papel, descricao_papel }: Request): Papel {
        const papel = this.papeisRepository.create({
            id_papel,
            chave_papel,
            descricao_papel
        });
        return papel;
    }
}

export default CreatePapelService;